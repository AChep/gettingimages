package com.artemchep.gettingimages.model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 * @author Artem Chepurnoy
 */
open class Model : RealmObject() {

    @PrimaryKey
    var phase: String? = null

    var imageUrl: String? = null

}