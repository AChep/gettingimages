package com.artemchep.gettingimages.search

import android.content.Context
import android.os.AsyncTask
import com.artemchep.gettingimages.Config
import com.artemchep.gettingimages.R
import com.artemchep.gettingimages.model.Model
import io.realm.Realm
import io.realm.RealmChangeListener
import io.realm.RealmResults
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStreamReader
import java.lang.ref.WeakReference
import java.net.HttpURLConnection
import java.net.URL


/**
 * @author Artem Chepurnoy
 */
class SearchPresenter(
        private val context: Context,
        private val view: SearchContract.View
) : SearchContract.Presenter, RealmChangeListener<RealmResults<Model>> {

    private lateinit var realm: Realm

    private val pendingPhases = HashSet<String>()

    override fun create() {
        // Open the realm for the UI thread.
        realm = Realm.getDefaultInstance()
    }

    override fun start() {
        realm.where(Model::class.java)
                .findAllAsync()
                .addChangeListener(this)

        // Set random hint text
        view.setSearchHint(context.resources.getStringArray(R.array.hints).let {
            it[(Math.random() * it.size).toInt()]
        })
    }

    override fun destroy() {
        realm.close() // Remember to close Realm when done.
    }

    override fun search(phase: String) {
        view.clearField()
        val p = phase.trim()
        GettyImagesLoader(p, this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
    }

    private fun notifyLoadingChanged() {
        view.setLoadingIndicator(pendingPhases.isNotEmpty())
    }

    private fun post(phase: String, url: String?) {
        if (url == null) {
            view.showToast(context.getString(R.string.help_error, phase))
        } else {
            // Create model object
            val model = Model()
            model.apply {
                this.phase = phase
                this.imageUrl = url
            }

            // Display it
            view.add(listOf(model))

            // Add to database
            realm.executeTransaction {
                realm.insert(model)
            }
        }
    }

    override fun onChange(t: RealmResults<Model>) {
        t.removeAllChangeListeners()
        view.add(t)
    }

    /**
     * @author Artem Chepurnoy
     */
    private class GettyImagesLoader(
            private val phase: String,
            presenter: SearchPresenter
    ) : AsyncTask<Unit, Unit, String?>() {

        /** Reference to host presenter */
        private val presenterRef = WeakReference<SearchPresenter>(presenter)

        override fun onPreExecute() {
            super.onPreExecute()
            presenterRef.get()?.run {
                pendingPhases.add(phase)
                notifyLoadingChanged()
            }
        }

        override fun doInBackground(vararg args: Unit): String? {
            return performGetRequest(phase)
        }

        private fun performGetRequest(phase: String): String? {
            val url = URL("https://api.gettyimages.com/v3/search/images?fields=id,title,thumb&sort_order=best&phrase=$phase")
            val con = url.openConnection() as HttpURLConnection
            con.run {
                requestMethod = "GET"
                setRequestProperty("Api-Key", Config.API_KEY)
            }

            val responseCode = con.responseCode

            return if (responseCode == HttpURLConnection.HTTP_OK) {
                var br: BufferedReader? = null
                try {
                    br = BufferedReader(InputStreamReader(con.inputStream))
                    val sb = StringBuilder()
                    while (true) {
                        val line = br.readLine()
                        if (line != null) {
                            sb.append(line)
                            sb.append('\n')
                        } else break
                    }
                    return extractImageUrl(sb.toString())
                } finally {
                    br?.close()
                }
            } else {
                null
            }
        }

        private fun extractImageUrl(json: String): String? {
            val image = JSONObject(json).optJSONArray("images")
                    .takeIf { it != null && it.length() > 0 }
                    ?.get(0) as? JSONObject
            val display = image?.optJSONArray("display_sizes")
                    .takeIf { it != null && it.length() > 0 }
                    ?.get(0) as? JSONObject
            return display?.optString("uri")
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            presenterRef.get()?.run {
                // Remove this phase from `loading`
                // ones.
                pendingPhases.remove(phase)
                notifyLoadingChanged()

                // Handle result
                post(phase, result)
            }
        }

    }
}