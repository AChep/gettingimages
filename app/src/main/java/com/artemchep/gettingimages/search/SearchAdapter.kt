package com.artemchep.gettingimages.search

import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.artemchep.gettingimages.R
import com.artemchep.gettingimages.model.Model
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

/**
 * @author Artem Chepurnoy
 */
class SearchAdapter(
        private val fragment: Fragment,
        private val list: List<Model>
) : RecyclerView.Adapter<SearchAdapter.ViewHolder>() {

    private val options = RequestOptions().centerCrop()

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val imageView = view.findViewById<ImageView>(R.id.image)
        val phaseTextView = view.findViewById<TextView>(R.id.phase)
    }

    override fun getItemCount(): Int = list.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val v = inflater.inflate(R.layout.item_query, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = list[position]
        holder.phaseTextView.text = model.phase
        // Load image
        Glide.with(fragment)
                .load(model.imageUrl)
                .apply(options)
                .into(holder.imageView)
    }

}