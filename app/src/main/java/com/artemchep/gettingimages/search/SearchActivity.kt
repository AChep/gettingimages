package com.artemchep.gettingimages.search

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.artemchep.gettingimages.R

class SearchActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var f: SearchFragment? = supportFragmentManager.findFragmentById(R.id.frame) as SearchFragment?
        if (f == null) {
            // Create the fragment
            f = SearchFragment()
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.frame, f)
                    .commit()
        }
    }

}
