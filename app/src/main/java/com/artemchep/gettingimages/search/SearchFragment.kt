package com.artemchep.gettingimages.search

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.Toast
import com.artemchep.gettingimages.R
import com.artemchep.gettingimages.model.Model


/**
 * @author Artem Chepurnoy
 */
class SearchFragment : Fragment(), SearchContract.View, View.OnClickListener {

    private lateinit var present: SearchContract.Presenter

    private lateinit var searchBtn: View
    private lateinit var emptyView: View
    private lateinit var recyclerView: RecyclerView
    private lateinit var progressView: View
    private lateinit var searchEditText: EditText

    private val list: MutableList<Model> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        present = SearchPresenter(context!!, this)
        present.create()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        progressView = view.findViewById(R.id.progress)
        recyclerView = view.findViewById(R.id.recycler)
        recyclerView.run {
            setHasFixedSize(true)
            layoutManager = GridLayoutManager(context!!, 2)
            adapter = SearchAdapter(this@SearchFragment, list)
        }
        emptyView = view.findViewById(R.id.empty)
        searchEditText = view.findViewById(R.id.field)
        searchEditText.run {
            setOnEditorActionListener({ p0, id, p2 ->
                if (id == EditorInfo.IME_ACTION_SEARCH) {
                    val src = searchEditText.text.toString()
                    present.search(src)
                    true
                } else false
            })
            addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(p0: Editable?) {
                    searchBtn.visibility = if (p0.isNullOrEmpty()) {
                        View.INVISIBLE
                    } else View.VISIBLE
                }

                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                }

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                }
            })
        }
        searchBtn = view.findViewById(R.id.search)
        searchBtn.run {
            setOnClickListener(this@SearchFragment)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        present.start()
    }

    override fun onDestroy() {
        present.destroy()
        super.onDestroy()
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.search -> {
                val src = searchEditText.text.toString()
                present.search(src)
            }
        }
    }

    override fun setLoadingIndicator(enabled: Boolean) {
        progressView.visibility = if (enabled) {
            View.VISIBLE
        } else View.GONE
    }

    override fun setSearchHint(hint: String) {
        searchEditText.hint = hint
    }

    override fun add(l: Collection<Model>) {
        if (l.isEmpty()) {
            return
        } else if (list.isEmpty()) {
            emptyView.visibility = View.GONE
        }

        // Add to recycler view and notify it
        val i = list.size
        list.addAll(l)
        recyclerView.adapter.notifyItemRangeInserted(i, l.size)
    }

    override fun showToast(text: String) {
        Toast.makeText(context!!, text, Toast.LENGTH_SHORT).show()
    }

    override fun clearField() {
        searchEditText.text = null
    }

}