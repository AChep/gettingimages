package com.artemchep.gettingimages.search

import com.artemchep.gettingimages.BasePresenter
import com.artemchep.gettingimages.BaseView
import com.artemchep.gettingimages.model.Model

/**
 * @author Artem Chepurnoy
 */
class SearchContract {

    interface View : BaseView {

        fun setLoadingIndicator(enabled: Boolean)

        fun setSearchHint(hint: String)

        /**
         * Adds collection of models to the
         * user interface.
         */
        fun add(l: Collection<Model>)

        fun showToast(text: String)

        fun clearField()

    }

    interface Presenter : BasePresenter {
        fun search(phase: String)
    }

}