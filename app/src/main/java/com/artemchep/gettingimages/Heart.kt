package com.artemchep.gettingimages

import android.app.Application
import io.realm.Realm

/**
 * @author Artem Chepurnoy
 */
class Heart : Application() {

    override fun onCreate() {
        super.onCreate()
        Realm.init(this)
    }

}