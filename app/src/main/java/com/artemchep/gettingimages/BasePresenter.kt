package com.artemchep.gettingimages

/**
 * @author Artem Chepurnoy
 */
interface BasePresenter {

    fun create()

    fun start()

    fun destroy()

}